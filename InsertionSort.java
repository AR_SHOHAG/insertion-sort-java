import java.util.Scanner;

class InsertionSort
{

	public void insertion_sort(int A[])
	{
		int i, j, n, key;
		for ( j = 1; j < A.length; j++ ) {
			key = A[j];
			i = j;

			while ( i > 0 && A[i-1] > key ) {
				A[i] = A[i-1];
				i = i - 1;
				//A[i] = key;
			}
			A[i] = key;

		}
	}

    public static void main(String args[])
    {
    int i_m, j_m, n_m, key_m, A[];

    Scanner in = new Scanner(System.in);
    System.out.println("Enter the number of elements of array");
    n_m = in.nextInt();
    A = new int[n_m];

    System.out.println("Enter " + n_m + " integers");

    for (i_m = 0; i_m < n_m; i_m++)
      A[i_m] = in.nextInt();

    InsertionSort I = new InsertionSort();

    I.insertion_sort(A);

    System.out.println("Array  after sorting: ");

	for( i_m = 0; i_m < A.length; i_m++)
		System.out.print(A[i_m] + " ");

	System.out.println();

	}
}